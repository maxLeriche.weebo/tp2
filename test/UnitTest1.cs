using System;
using Xunit;

using prog;

namespace test
{
    public class UnitTest1
    {
        //partie tableau
        [Fact]
        public void test_de_chaine()
        {
            string tab;
            tab="Masterpiecetestertothemoon.";
            Assert.Equal(11,Program.chaine(tab));
            tab="Masterpiecetestertothemoon";
            Assert.Equal(10,Program.chaine(tab));
            tab="masterpiecetestertothemoon.";
            Assert.Equal(01,Program.chaine(tab));
            tab="masterpiecetestertothemoon";
            Assert.Equal(00,Program.chaine(tab));
            tab="Masterpiecetestertothemoon!";
            Assert.Equal(10,Program.chaine(tab));
            tab="éasterpiecetestertothemoon:";
            Assert.Equal(00,Program.chaine(tab));
            tab=" asterpiecetestertothemoon ";
            Assert.Equal(00,Program.chaine(tab));
        }
        [Fact]
        public void test_find_tableaux()
        {
            int[] tableau={0,1,2,3,4,5,6,7,8,9};
            Assert.Equal(2,Program.find_tab(tableau,2));
            tableau[0]=0;
            tableau[1]=0;
            tableau[2]=0;
            tableau[3]=0;
            tableau[4]=0;
            tableau[5]=0;
            tableau[6]=0;
            tableau[7]=0;
            tableau[8]=0;
            tableau[9]=0;
            Assert.Equal(10,Program.find_tab(tableau,1));
            tableau[0]=1;
            tableau[1]=1;
            tableau[2]=1;
            tableau[3]=1;
            tableau[4]=1;
            tableau[5]=1;
            tableau[6]=1;
            tableau[7]=1;
            tableau[8]=1;
            tableau[9]=1;
            Assert.Equal(-1,Program.find_tab(tableau,1));
            tableau[0]=1;
            tableau[1]=1;
            tableau[2]=1;
            tableau[3]=1;
            tableau[4]=1;
            tableau[5]=1;
            tableau[6]=1;
            tableau[7]=1;
            tableau[8]=1;
            tableau[9]=100000000;
            Assert.Equal(9,Program.find_tab(tableau,100000000));
        }
    }
}
